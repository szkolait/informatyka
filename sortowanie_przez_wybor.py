def znajdzminideks(lista):
    min = lista[0]
    minindex = 0
    for i in range(1, len(lista)):
        if lista[i] < min:
            min = lista[i]
            minindex = i
    return minindex

def sortowanieprzezwybieranie(lista):
    nowa = []
    for i in range(len(lista)):
        minindex = znajdzminideks(lista)
        nowa.append(lista.pop(minindex))
    return nowa

lista = [3, 5, 2, 1, 4]
print(sortowanieprzezwybieranie(lista))
