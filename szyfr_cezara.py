def szyfruj(haslo, klucz):
    alfabet = "abcdefghijklmnopqrstuvwxyz"
    zaszyfrowane = ""
    for literka in haslo:
        if literka.isalpha():
            if literka.isupper():
                zaszyfrowany_znak = alfabet[(alfabet.index(literka.lower()) + klucz) % 26].upper()
            else:
                zaszyfrowany_znak = alfabet[(alfabet.index(literka) + klucz) % 26]
            zaszyfrowane += zaszyfrowany_znak
        else:
            zaszyfrowane += literka
    return zaszyfrowane

haslo = input("Podaj hasło do zaszyfrowania: ")
klucz = int(input("Podaj klucz szyfrowania: "))

zaszyfrowane_haslo = szyfruj(haslo, klucz)
print("Zaszyfrowane hasło:", zaszyfrowane_haslo)
